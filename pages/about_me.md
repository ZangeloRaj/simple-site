% about me
% /dev/urandom
% july 2020

**name**: /dev/urandom (or `dev` for short, sometimes `rnd`)

**date of birth**: june 1993

**gender**: no idea, think i might have lost it a few years ago

**job**: programmer

**tech**: i'm mostly a computing and retro software/gaming dork. i am kind of a
linux/open-source fan, although i try not to be a jerk about it as some do. i
occasionally code something in C or C++ in my personal time.

**politics**: i'm somewhere in the middle between progressive liberalism, social
democracy and libertarian socialism. i try to appreciate different people's
approaches to improving their societies, even if i disagree about the details.
having said that, i do not have any love for fascists, reactionaries, social
conservatives or bigots of any kind, as well as any viewpoint that values
ideology over actually solving problems.

**religion**: i am an agnostic/atheist, but i try to respect other people's
religious or spiritual beliefs (as long as these beliefs don't infringe on
other people's freedoms or aren't enforced on others politically or socially).
